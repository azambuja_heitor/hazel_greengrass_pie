# Lambda function that subscribes to core shadow and updates the csv containing 
# parameters for the collector local function.


import greengrasssdk
import platform
import logging
import csv
import json


client = greengrasssdk.client('iot-data')
volumePath = '/dest/modbus'
field_names = ['halt', 'method', 'collect_interval', 'ip', 'port', 'baudrate', 'parity', 'stopbits', 'first_holding_register', 'holding_registers_amount', 'first_coil', 'coils_amount', 'timeout', 'unit']

def function_handler(event, context):

	shadow = json.loads(event)
	logging.info(shadow)

	protocol_method = shadow.state.reported.protocol_method #'tcp'
	collect_interval = shadow.state.reported.collect_interval #'20'
	holding_register_initial_address = shadow.state.reported.holding_register_initial_address #'0'
	amount_of_holding_registers = shadow.state.reported.amount_of_holding_registers #'8'
	coil_initial_address = shadow.state.reported.coil_initial_address #'0'
	amount_of_coils = shadow.state.reported.amount_of_coils #'8'
	time_out = shadow.state.reported.time_out #'15'
	halt = shadow.state.reported.halt #'0'
	device_unit = shadow.state.reported.device_unit #'1'
	usb_port = shadow.state.reported.usb_port #'tty/USB0'
	baud = shadow.state.reported.baud #'9600'
	parity = shadow.state.reported.parity #'N'
	stop_bits = shadow.state.reported.stop_bits #'1'
	ip_address = shadow.state.reported.ip_address #'169.254.252.78'


	try:
		with open(volumePath + '/parameters.csv', 'w') as write_file:
			writer = csv.DictWriter(write_file, fieldnames='field_names')
			writer.writeheader()
			writer.writerow({
				'halt': halt,
				'method': protocol_method,
				'collect_interval': collect_interval,
				'ip': ip_address,
				'port': usb_port,
				'baudrate': baud,
				'parity': parity,
				'stopbits': stop_bits,
				'first_holding_register': holding_register_initial_address,
				'holding_registers_amount': amount_of_holding_registers,
				'first_coil': coil_initial_address,
				'coils_amount': amount_of_coils,
				'timeout': time_out,
				'unit': device_unit
			})
	except Exception as e:
		logging.error("Experiencing error :{}".format(e))
		client.publish(topic='hazelpie/parameters_updater', payload=json.dumps({'message': 'Unable to change parameters!'}))
	else:
		client.publish(topic='hazelpie/parameters_updater', payload=json.dumps({'message': 'Parameters updates successfully!'}))


'''def function_handler(event, context):

	logging.info(event)

	protocol_method = event["current"]["state"]["reported"]["method"]
	collect_interval = event["current"]["state"]["reported"]["collect_interval"]
	holding_register_initial_address = event["current"]["state"]["reported"]["first_holding_register"]
	amount_of_holding_registers = event["current"]["state"]["reported"]["holding_registers_amount"]
	coil_initial_address = event["current"]["state"]["reported"]["first_coil"]
	amount_of_coils = event["current"]["state"]["reported"]["coils_amount"]
	time_out = event["current"]["state"]["reported"]["timeout"]
	halt = event["current"]["state"]["reported"]["halt"]
	device_unit = event["current"]["state"]["reported"]["unit"]
	usb_port = event["current"]["state"]["reported"]["port"]
	baud = event["current"]["state"]["reported"]["baudrate"]
	parity = event["current"]["state"]["reported"]["parity"]
	stop_bits = event["current"]["state"]["reported"]["stopbits"]
	ip_address = event["current"]["state"]["reported"]["ip"]

	try:
		with open(volumePath + '/parameters.csv', 'w') as write_file:
			writer = csv.DictWriter(write_file, fieldnames='field_names')
			writer.writeheader()
			writer.writerow({
				'halt': halt,
				'method': protocol_method,
				'collect_interval': collect_interval,
				'ip': ip_address,
				'port': usb_port,
				'baudrate': baud,
				'parity': parity,
				'stopbits': stop_bits,
				'first_holding_register': holding_register_initial_address,
				'holding_registers_amount': amount_of_holding_registers,
				'first_coil': coil_initial_address,
				'coils_amount': amount_of_coils,
				'timeout': time_out,
				'unit': device_unit
			})
	except Exception as e:
		logging.error("Experiencing error :{}".format(e))
		client.publish(topic='hazelpie/parameters_updater', payload=json.dumps({'message': 'Unable to change parameters!'}))
	else:
		client.publish(topic='hazelpie/parameters_updater', payload=json.dumps({'message': 'Parameters updates successfully!'}))

'''