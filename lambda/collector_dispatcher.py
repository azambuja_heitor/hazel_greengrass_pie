####################################################################################
#
#    Function created on top of amazon example.
#    It should connect to a clp trough modbus tcp protocol and post an mqtt message
#
####################################################################################
#
# Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#

# greengrassHelloWorldCounter.py
# Demonstrates a simple publish to a topic using Greengrass core sdk
# This lambda function will retrieve underlying platform information and send a hello world message along with the
# platform information to the topic 'hello/world/counter' along with a counter to keep track of invocations.
#
# This Lambda function requires the AWS Greengrass SDK to run on Greengrass devices. This can be found on the AWS IoT Console.

from pymodbus.client.sync import ModbusTcpClient
# from datetime import date, datetime
import greengrasssdk
import platform
import time
import json

from __future__ import print_function

# Creating a greengrass core sdk client
client = greengrasssdk.client('iot-data')

# Retrieving platform information to send from Greengrass Core
my_platform = platform.platform()

# Counter to keep track of invocations of the function_handler
my_counter = 0


def function_handler(event, context):

    modbus = ModbusTcpClient('169.254.252.78', timeout=15)
    modbus.connect()
#   modbus.write_coil(1, True)
    coils = modbus.read_coils(0, 10, unit=1)
#   assert(coils.function_code < 0x80)
    holding_reg = modbus.read_holding_registers(0, 10, unit=1)
#   assert(holding_reg.function_code < 0x80)

    modbus.close()

    # REQUIRES LOCAL DATA BACKUP
    print(coils.bits[0:])
    print(holding_reg.registers)


    global my_counter
    my_counter = my_counter + 1
    print("This is a test!")
    if not my_platform:
        client.publish(
            topic='hello/world/counter',
            payload=json.dumps({'message': 'Hello world! Sent from Greengrass Core.  Invocation Count: {}. Coils: {}. Holding Registers: {}'.format(my_counter, coils.bits[0:], holding_reg.registers)})
        )
    else:
        client.publish(
            topic='hello/world/counter',
            payload=json.dumps({'message': 'Hello world! Sent from Greengrass Core running on platform: {}.  Invocation Count: {}. Coils: {}. Holding Registers: {}'
                                .format(my_platform, my_counter, coils.bits[0:], holding_reg.registers)})
        )
    time.sleep(20)
    return
