# Dispatcher function. Reads the csv containing the collected data and posts to mqtt topic

# TODO: convert print statements in logging;
#		implement last_collected parameter in core_shadow;

import sys
import greengrasssdk
import platform
import os
import logging
import csv
#from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient

# Create a shadow client
#shadow_client = AWSIoTMQTTShadowClient('iot-shadow')
# Create a Greengrass Core SDK client.

client = greengrasssdk.client('iot-data')
volumePath = '/dest/modbus'

#core_shadow_update_topic = '$aws/things/hazelpie_greengrass_Core/shadow/update'

data = []

def function_handler(event, context):

	#logger.info(event)

#	shadow_client = AWSIoTMQTTShadowClient(clientId)

	#shadow_client.configureAutoReconnectBackoffTime(1, 32, 20)
	#shadow_client.configureConnectDisconnectTimeout(10)  # 10 sec
	#shadow_client.configureMQTTOperationTimeout(5)  # 5 sec

#	shadow_client.connect()
	#shadow handler
#	shadow_handler = shadow_client.createShadowHandler(True)

	# Reads last_collected timestamp from shadow

	# Reads last_collected timestamp from local csv
	try:
		if os.path.isfile(volumePath + '/last_timestamp_read.csv') is False:  # If there is no last read, reads all the data output
			with open(volumePath + '/data_output.csv', 'r') as read_file:
				reader = csv.DictReader(read_file, delimiter=';')

				for row in reader:  # every data is new! Append all entries to data dictionary
					# convert print statements into loggin
					print('Newly collected data:')
					print(row)
					data.append(row)
					last_timestamp = row['timestamp']
						
			client.publish(topic='hazelpie/collected_data', payload=data)

			with open(volumePath + '/last_timestamp_read.csv', 'w') as write_file:  # Saves timestamp from last data. Will be a shadow parameter in GGC.
				writer = csv.DictWriter(write_file, fieldnames=['last_read'])
				writer.writeheader()
				print(last_timestamp)
				writer.writerow({'last_read': last_timestamp})
		else:
			with open(volumePath + '/last_timestamp_read.csv', 'r') as read_timestamp:  # reads timestamp from last read
				reader = csv.DictReader(read_timestamp)
				row1 = next(reader)
				last_timestamp = row1['last_read']

			with open(volumePath + '/data_output.csv', 'r') as read_file:
				reader = csv.DictReader(read_file, delimiter=';')

				for row in reader:  # interates over the data until it finds the last collected timestamp
					if row['timestamp'] == last_timestamp:
						break
						#  Every line from here is a new data!
				for row in reader:  # appends every new data to dictionary.
					# convert print statement into loggin
					print('Newly collected data:')
					print(row)
					data.append(row)

					last_timestamp = row['timestamp']

			if not data:  # if data string is empty
				pass
			else:  # publishes to topic
				client.publish(topic='hazelpie/collected_data', payload=data)
				# writes last_timestamp to shadow
				#shadow_handler.shadowUpdate

				# writes last_timestamp to local csv
				with open(volumePath + '/last_timestamp_read.csv', 'w') as write_file: 
					writer = csv.DictWriter(write_file, fieldnames=['last_read'])
					writer.writeheader()
					print(last_timestamp)
					writer.writerow({'last_read': last_timestamp})

	except Exception as e:
		logging.error("Experiencing error :{}".format(e))


'''
	client.publish(topic='hazelpie/data', payload='Sent from AWS Greengrass Core')
	try:
		volumeInfo = os.stat(volumePath)
		client.publish(topic='hazelpie/data', payload=str(volumeInfo))
		with open(volumePath + '/data_output.csv', 'r') as output:
			output.write('successfully write to a file.\n')
		with open(volumePath + '/test', 'r') as myfile:
			data = myfile.read()
		client.publish(topic='LRA/test', payload=data)
	except Exception as e:
		logging.error("Experiencing error :{}".format(e))
'''