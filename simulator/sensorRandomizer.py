'''

	Class that randomizes sensors in defferent efficiency ranges

		Sensors receive a full range and a good range of values, bad range
	is calculated from them. *Must implement casa in wich good range is
	one of the extremes.

	list of sensors:
		- PH
		- Flow

'''
import random


class randomSensor():

	numberOfItems = 1  # Number of values to be generated
	floatPrecision = 1  # Float precision as integer, used in define range method
	floatPrecisionStr = "{:.1f}"  # Float precision as string
	sensorList = list()  # randomly generated sensor reads
	fullRange = [0, 100]  # Full range of possible reads
	goodRange = [26, 76]  # Good range of possible reads
	upBadRange = list()
	lowBadRange = list()

	def __init__(self):
		self.defineBadRange()

	def setParameters(self, NumberOfItems=numberOfItems, FloatPrecision=floatPrecision):
		self.numberOfItems = NumberOfItems
		self.floatPrecision = FloatPrecision
		self.floatPrecisionStr = "{:." + str(FloatPrecision) + "f}"
		self.defineBadRange()
		return

	def generate(self, rangeList):
		del self.sensorList[:]
		for x in range(self.numberOfItems):
			self.sensorList.append(self.floatPrecisionStr.format(random.uniform(rangeList[0], rangeList[1])))
		return self.sensorList

	def defineBadRange(self):
		badStr = '0.'

		del self.upBadRange[:]
		del self.lowBadRange[:]

		for x in range(self.floatPrecision):
			if x == self.floatPrecision - 1:
				badStr = badStr + '1'
			else:
				badStr = badStr + '0'
		badFloat = float(badStr)

		self.upBadRange.append(self.goodRange[1] + badFloat)
		self.upBadRange.append(self.fullRange[1])
		self.lowBadRange.append(self.fullRange[0])
		self.lowBadRange.append(self.goodRange[0] - badFloat)
		return

	def setRange(self, FullRange=fullRange, GoodRange=goodRange):
		self.fullRange = FullRange
		self.goodRange = GoodRange
		self.defineBadRange()
		return

	def normal(self):
		return self.generate(self.fullRange)

	def good(self):
		return self.generate(self.goodRange)

	def upBad(self):
		return self.generate(self.upBadRange)

	def lowBad(self):
		return self.generate(self.lowBadRange)

	def bad(self):
		randomFlag = bool(random.getrandbits(1))
		if randomFlag is True:
			return self.upBad()
		else:
			return self.lowBad()


class phSensor(randomSensor):
	def __init__(self):
		self.setRange(FullRange=[0, 14], GoodRange=[4.5, 6.5])


class flowSensor(randomSensor):
	def __init__(self):
		self.setRange(FullRange=[0, 6], GoodRange=[2, 3.5])


def randCoils(numberOfCoils=1):
	randomList = list()
	for x in range(numberOfCoils):
		randomList.append(bool(random.getrandbits(1)))
	return randomList


sensor = randomSensor()
print(sensor.upBadRange)
print(sensor.lowBadRange)
print(sensor.bad())
print(sensor.normal())
print(sensor.good())


randomizedCoils = randCoils(10)
# print randomizedCoils
