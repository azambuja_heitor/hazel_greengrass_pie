'''
        Implementation of modbus server
'''
############################################
# Import various server implementations
############################################
from pymodbus.server.async import StartTcpServer
from pymodbus.server.async import StartSerialServer

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusAsciiFramer


##############################################
# Configure Logging service
##############################################
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


##############################################
# Initialize data story
##############################################
store = ModbusSlaveContext(
        di = ModbusSequentialDataBlock(0, [17]*100), # Discrete Input
        co = ModbusSequentialDataBlock(0, [17]*100), # Coils Output, Discrete Output
        hr = ModbusSequentialDataBlock(0, [17]*100), # Holding Registers, Analog Output
        ir = ModbusSequentialDataBlock(0, [17]*100)) # Input Registers, Analog Input

# When single flag is set to true, context will be the same for any unit id
context = ModbusServerContext(slaves = store, single = True)

##############################################
# Initialize servr information
##############################################
serverID = ModbusDeviceIdentification()
serverID.VendorName = 'Hazel'
serverID.ProductCode = 'HZMBS01'
serverID.VendorUrl = 'http://hazelrisk.com'
serverID.ProductName = 'Modbus Server Simulator'
serverID.ModelName = 'ModNut'
serverID.MajorMinorRevision = '1.0'

##############################################
# Run the server
##############################################
StartTcpServer(context, identity = serverID, address = ('192.168.0.12', 502))
'''
from twisted.internet import reactor
StartTcpServer(context, identity=serverID, address=('127.0.1.2',502), defer_reactor_run=True)
reactor.run()
'''
#StartTcpServer(context, identity = serverID, framer = ModbusRtuFramer, address = ("127.0.1.1"$
#StartSerialServer(context, identity = serverID, port = 'ttyS0', framer = ModbusRtuFramer)
