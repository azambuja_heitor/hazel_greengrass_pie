#
#  Appends data (real timestamp, randomized coils and registers) to CSV
#  file and prints contents of the file to terminal.
#
import csv
import datetime
import pytz
import os
import random

''' # CSV INITIAL TEST
with open('test.csv', 'r') as parameters_file:
	parameters = csv.DictReader(parameters_file)

	row1 = next(parameters)
	par_value = int(row1['parameter2'])
	print(str(par_value) + '+1=' + str(par_value + 1))

	for row in parameters:
		print(row['parameter1'], row['parameter2'], row['parameter3'])
'''


# Prints contents of csv file
def print_csv(file_path, delimiter_type=','):
	with open(file_path, 'r') as read_file:
		reader = csv.DictReader(read_file, delimiter=delimiter_type)
		for row in reader:
			print(row)


# Randomizes registers
def rand_reg(amount):
	string = ''
	for i in range(0, amount):
		if i != 0:
			string = string + ','
		string = string + str(random.randint(1, 500))
	return string


# Randomizes cois
def rand_coil(amount):
	values = ['True', 'False']
	string = ''
	for i in range(0, amount):
		if i != 0:
			string = string + ','
		string = string + random.choice(values)
	return string


def get_timestamp(timezone):
	tm = datetime.datetime.now(tz=pytz.timezone(timezone))
	timestamp = tm.isoformat()
	return timestamp


''' # would be useful
def append_csv(file_path, field_names, delimiter_type=',', data_to_write):

'''

field_names = ['timestamp', 'holding_registers', 'coils']
tz = 'Brazil/East'
timestamp = get_timestamp(tz)
holding_registers = rand_reg(6)
coils = rand_coil(6)

if os.path.isfile('data_output.csv') is False:
	with open('data_output.csv', 'w') as out_file:
		writer = csv.DictWriter(out_file, fieldnames=field_names, delimiter=';')

		writer.writeheader()
		writer.writerow({'timestamp': timestamp, 'holding_registers': holding_registers, 'coils': coils})
else:
	with open('data_output.csv', 'a') as out_file:
		writer = csv.DictWriter(out_file, fieldnames=field_names, delimiter=';')
		writer.writerow({'timestamp': timestamp, 'holding_registers': holding_registers, 'coils': coils})

print_csv('data_output.csv', delimiter_type=';')

''' # Replaced by print_csv function.
with open('data_output.csv', 'r') as read_file:
	reader = csv.DictReader(read_file, delimiter=';')
	print(field_names)
	for row in reader:
		print(row[field_names[0]], row[field_names[1]], row[field_names[2]])
'''


