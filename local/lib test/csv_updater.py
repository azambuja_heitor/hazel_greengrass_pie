import csv

value1 = 'some data'
value2 = 200

with open('par.csv', 'r') as csv_file:
	reader = csv.DictReader(csv_file)
	fieldnames = reader.fieldnames
with open('par.csv', 'w') as csv_file:
	writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
	writer.writeheader()
	writer.writerow({fieldnames[0]: value1, 'parameter2': value2})


