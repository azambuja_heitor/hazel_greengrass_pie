#
#  Reads CSV data and appends to JSON
#
import json
import csv
import os

''' # INITIAL TEST. Wanted to use JSON for local data storage, but was unable to append to file properly. Decided to use CSV for local save
tm = datetime.datetime.now(tz=pytz.timezone('Brazil/East'))
timestamp = tm.isoformat()

data = [{
	'timestamp': timestamp,
	'holding_registers': 'holding_registers.registers',
	'coils': 'coils.bits[0:]'
}]

with open('test.json', 'a') as output_file:
	json.dump(data, output_file,indent=2)

with open('test.json', 'r') as read_file:
	data = json.load(read_file)

for row in data:
	print(row['timestamp'])
'''
# initiates json dictionary

if os.path.isfile('data.json') is True:  # If data.json already exists, loads it in data dictionary
	with open('data.json') as file:
		data = json.load(file)
else:  # else creates data dictionary
	data = {"collected_data": []}

# print(data['data'])
if os.path.isfile('last_timestamp_read.csv') is False:  # If there is no last read, reads all the data output
	with open('data_output.csv', 'r') as read_file:
		reader = csv.DictReader(read_file, delimiter=';')

		for row in reader:  # every data is new! Append all entries to data dictionary
				print('Newly collected data:')
				print(row)
				data['collected_data'].append(row)
				last_timestamp = row['timestamp']

		with open('last_timestamp_read.csv', 'w') as write_file:  # Saves timestamp from last data. Will be a shadow parameter in GGC.
			writer = csv.DictWriter(write_file, fieldnames=['last_read'])
			writer.writeheader()
			# print(last_timestamp)
			writer.writerow({'last_read': last_timestamp})
else:  # a last read timestamp exists 
	with open('last_timestamp_read.csv', 'r') as read_timestamp:  # reads timestamp from last read
		reader = csv.DictReader(read_timestamp)
		row1 = next(reader)
		last_timestamp = row1['last_read']

	with open('data_output.csv', 'r') as read_file:
		reader = csv.DictReader(read_file, delimiter=';')

		for row in reader:  # interates over the data until it finds the last collected timestamp
			if row['timestamp'] == last_timestamp:
				break
				#  Every line from here is a new data!
		for row in reader:  # appends every new data to dictionary.
			print('Newly collected data:')
			print(row)
			data['collected_data'].append(row)
			
			last_timestamp = row['timestamp']

		with open('last_timestamp_read.csv', 'w') as write_file: # Saves timestamp from last data
			writer = csv.DictWriter(write_file, fieldnames=['last_read'])
			writer.writeheader()
			# print(last_timestamp)
			writer.writerow({'last_read': last_timestamp})

print('No more data to log.')

with open('data.json', 'w') as write_json:  # writes dictionary to json
	json.dump(data,write_json,indent=2)

'''
for item in data['data']:
	print(item['timestamp'])
'''

'''
tz = 'Brazil/East'
timestamp = get_timestamp(tz)
print(timestamp)
'''
