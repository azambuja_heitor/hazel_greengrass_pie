#!/usr/bin/env python
from __future__ import print_function
import time
import datetime
#import pytz
import csv
import os
# import json

'''
# AWS libs
import greengrasssdk
import platform
import json
'''
# Modbus TCP lib
from pymodbus.client.sync import ModbusTcpClient
# Modbus RTU libs
# import pymodbus
# from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
# serial RTU client instance
# from pymodbus.transaction import ModbusRtuFramer

import logging

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)

# Modbus client
# modbus = ModbusClient(method='rtu', port='/dev/ttyUSB0', baudrate=19200, timeout=1, parity='N', bytesize=8, stopbits=1)
core_id = '5'
message_version = '1'

def collector():

	#  ip = '169.254.252.78'
	try:  # tries to load parameters from parameters file
		with open('/src/modbus/parameters.csv', 'r') as parameters_file:
			reader = csv.DictReader(parameters_file)
			parameters = next(reader)

			protocol_method = parameters['method']
			collect_interval = int(parameters['collect_interval'])
			holding_register_initial_address = int(parameters['first_holding_register'])
			amount_of_holding_registers = int(parameters['holding_registers_amount'])
			coil_initial_address = int(parameters['first_coil'])
			amount_of_coils = int(parameters['coils_amount'])
			time_out = int(parameters['timeout'])
			halt = int(parameters['halt'])
			device_unit = int(parameters['unit'])

			if protocol_method == 'rtu':
				usb_port = parameters['port']
				baud = int(parameters['baudrate'])
				parity = parameters['parity']
				stop_bits = int(parameters['stopbits'])

				modbus = ModbusClient(method=protocol_method, port=usb_port, baudrate=baud, timeout=time_out, parity=parity, stopbits=stop_bits, bytesize=8)
			elif protocol_method == 'tcp':
				ip_address = parameters['ip']
				modbus = ModbusTcpClient(ip_address, timeout=time_out, port=502)
	except Exception as error:
		print('Unable to open parameters file. Trying again in one minute')
		print(error)
		time.sleep(60)
		collector()
		# modbus = ModbusClient(method = 'rtu', port = '/dev/ttyUSB0', baudrate = 19200, timeout=1,parity='N', bytesize=8, stopbits=1)
	else:

		if halt == 1:  # halt collector
			print('Collector is halted! trying again in one minute.')
			time.sleep(60)
			collector()
		else:

			try:
				modbus.connect()
			except Exception as error:
				print(error)
				print('The device was unable to connect to the PLC trough the Modbus ' + protocol_method + ' method! Trying again in one minute')
				time.sleep(60)
				collector()
			else:
				read_holding_registers = modbus.read_holding_registers(holding_register_initial_address, amount_of_holding_registers, unit=device_unit)
				read_coils = modbus.read_coils(coil_initial_address, amount_of_coils, unit=device_unit)

				modbus.close()

				holding_registers = read_holding_registers.registers
				coils = read_coils.bits[0:]

				tm = datetime.datetime.now()
				timestamp = tm.isoformat()

				field_names = ['timestamp', 'holding_registers', 'holding_registers_address', 'coils', 'coils_address', 'core_id', 'message_version' ]

				initial_address = 2

				holding_registers_address = []

				for i in range(amount_of_holding_registers):
					holding_registers_address.append(i + holding_register_initial_address)

				coils_address = []

				for i in range(amount_of_coils):
                                        coils_address.append(i + coil_initial_address)

				try:  # Tries to save at data_output.csv
					if os.path.isfile('/src/modbus/data_output.csv') is False:  # if the file doesn't exists, create file and save data
						with open('/src/modbus/data_output.csv', 'w') as out_file:
							writer = csv.DictWriter(out_file, fieldnames=field_names, delimiter=';')

							writer.writeheader()
							writer.writerow({'timestamp': timestamp, 'holding_registers': holding_registers, 'holding_registers_address': holding_registers_address, 'coils': coils, 'coils_address': coils_address, 'core_id': core_id, 'message_version': message_version})
					else:  # Save data
						with open('/src/modbus/data_output.csv', 'a') as out_file:
							writer = csv.DictWriter(out_file, fieldnames=field_names, delimiter=';')
							writer.writerow({'timestamp': timestamp, 'holding_registers': holding_registers, 'holding_registers_address': holding_registers_address, 'coils': coils, 'coils_address': coils_address, 'core_id': core_id, 'message_version': message_version})
				except Exception as error:  # If it fails, trows an error and saves at data_timestamp.csv
					print('Unable to save to "data_output.csv" due to an error')
					print(error)
					print('Saving to "data_"+timestamp".csv".')
					new_file_name = 'data_' + timestamp + '.csv'
					with open(new_file_name, 'w') as out_file:  # Saves at new csv file
						writer = csv.DictWriter(out_file, fieldnames=field_names, delimiter=';')

						writer.writeheader()
						writer.writerow({'timestamp': timestamp, 'holding_registers': holding_registers, 'holding_registers_address': holding_registers_address ,'coils': coils, 'coils_address': coils_address, 'core_id': core_id, 'message_version': message_version})

				time.sleep(collect_interval)
				collector()


# Runs collector function
collector()
