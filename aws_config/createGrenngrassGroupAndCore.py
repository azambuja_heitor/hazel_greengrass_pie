##############################################################
#	This script creates a greengrass group with core.
#	It takes the Subscription, resource and function 
# definitions from a reference core. 
##############################################################
import boto3
import json
import ast
import logging
import datetime
import zipfile
from os import remove

######################################################
# create logger
######################################################
logger = logging.getLogger("createGreengrassGroupAndCore.py")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

logger.info("Program started.")

ggClient = boto3.client('greengrass')
iotClient = boto3.client('iot')

#####################################################
# Constants
#####################################################
groupName = "Group01_TestStation"
coreName = "Core01_TestStation"
referenceGroupId = '4d0d08da-ca26-4249-9cf9-820e937d682d'
ggHost = "greengrass.iot.us-east-1.amazonaws.com"


#####################################################
# Functions
#####################################################

def serializeDatetime(o):
	if isinstance(o, datetime.datetime):
		return o.__str__()


def treatAndLogDict(var, functionName=''):
	outputVar = ast.literal_eval(json.dumps(response))
	debugStr = functionName + " function payload: {}"
	logger.debug(debugStr.format(json.dumps(outputVar, indent=2)))
	return outputVar

#####################################################
# Defining the Greengrass Core Device in the cloud
#####################################################
logger.info("Defining the Greengrass Core Device in the cloud")

# Create a Thing for the GGC
response = iotClient.create_thing(thingName=coreName)
var = treatAndLogDict(response, functionName="create_thing")
#var = ast.literal_eval(json.dumps(response))
#logger.debug("create_thing function payload: {}".format(json.dumps(var, indent=2)))

thingName = var["thingName"]
thingArn = var["thingArn"]
thingId = var["thingId"]

# Create a certificate to identify the GGC
response = iotClient.create_keys_and_certificate(setAsActive=True)
var = treatAndLogDict(response, functionName="create_keys_and_certificate")


certificateArn = var["certificateArn"]
certificateId = var["certificateId"]
certificatePem = var["certificatePem"]
#logger.debug("certificatePem = {}".format(certificatePem))
publicKey = var["keyPair"]["PublicKey"]
#logger.debug("publicKey = {}".format(publicKey))
privateKey = var["keyPair"]["PrivateKey"]
#logger.debug("privateKey = {}".format(privateKey))

fileName = certificateId[0 : 9]
certFileName = fileName + '.cert.pem'
privateFileName = fileName + '.private.key'
publicFileName = fileName + '.public.key'

fileList = [certFileName, privateFileName, publicFileName, 'config.json']

# # create certificate files
logger.info("Creating certificate files.")
with open(certFileName, 'w') as pemFile, open(privateFileName, 'w') as privateFile, open(publicFileName, 'w') as publicFile:
	pemFile.write(certificatePem)
	privateFile.write(privateKey)
	publicFile.write(publicKey)

response = iotClient.describe_endpoint()
responseDict = treatAndLogDict(response, functionName="describe_endpoint")
iotHost = responseDict["endpointAddress"]

configData = {
  	"coreThing" : {
    	"caPath" : "root.ca.pem",
   		"certPath" : certFileName,
    	"keyPath" : privateFileName,
 	   "thingArn" : thingArn,
    	"iotHost" : iotHost,
    	"ggHost" : ggHost,
    	"keepAlive" : 600
  	},
  	"runtime" : {
    	"cgroup" : {
    		"useSystemd" : "yes"
    	}
  	},
  	"managedRespawn" : False
}

with open('config.json', 'w') as configFile:
	json.dump(configData, configFile, indent=2)

logger.info("Zipping certificate files.")

with zipfile.ZipFile(fileName+'.zip', mode='w') as zipF:
	for file in fileList:
		zipF.write(file)
		remove(file)

# Get the policy with the permissions for the GGC, if it doens't exist, create it
newPolicyName = "GreengrassCorePolicy"

try:
	response = iotClient.get_policy(policyName=newPolicyName)
	var = ast.literal_eval(json.dumps(response, default=serializeDatetime))
	logger.debug("get_policy function payload: {}".format(json.dumps(var, indent=2)))

	policyName = var["policyName"]
	policyArn = var["policyArn"]
	policyVersionId = var["defaultVersionId"]

except Exception as e:
	logger.error("Error on get_policy: {}".format(e))
	logger.info("Policy doesn't exists.")
	logger.info("Creating Policy.")

	newPolicyDocument = '''{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Action":["iot:*","greengrass:*"],"Resource":"*"}]}'''
	
	response = iotClient.create_policy(policyName=newPolicyName, policyDocument=newPolicyDocument)
	var = treatAndLogDict(response, functionName="create_policy")

	policyName = var["policyName"]
	policyArn = var["policyArn"]
	policyVersionId = var["policyVersionId"]

# Attach policy to the GGC Certificate
iotClient.attach_principal_policy(policyName=policyName, principal=certificateArn)

# Attach GGC certificate to the GGC Thing
iotClient.attach_thing_principal(thingName=thingName, principal=certificateArn)

#############################################################
# Creating a Greengrass Deployment
#############################################################
logger.info("Creating a Greengrass Deployment")

# create a Core Definition
response = ggClient.create_core_definition(Name=coreName)
var = treatAndLogDict(response, functionName="create_core_definition")
coreId = var["Id"]

cores = [
	{
		"CertificateArn": certificateArn,
		"Id": coreName + '_v1',
		"SyncShadow": True,
		"ThingArn": thingArn
	}
]

response = ggClient.create_core_definition_version(CoreDefinitionId=coreId, Cores=cores)
var = treatAndLogDict(response, functionName="create_core_definition_version")

coreDefinitionArn = var["Arn"]

# Get the other definitions from reference core
response = ggClient.list_group_versions(GroupId=referenceGroupId, MaxResults='1')
var = treatAndLogDict(response, functionName="list_group_versions")

referenceGroupVersionId = var["Versions"][0]["Version"]

response = ggClient.get_group_version(GroupId=referenceGroupId, GroupVersionId=referenceGroupVersionId)
var = treatAndLogDict(response, functionName="get_group_version")

functionDefinition = var["Definition"]["FunctionDefinitionVersionArn"]
subscriptionDefinition = var["Definition"]["SubscriptionDefinitionVersionArn"]
resourceDefinition = var["Definition"]["ResourceDefinitionVersionArn"]

# Create an AWS Greengrass Group
response = ggClient.create_group(Name=groupName)
var = treatAndLogDict(response, functionName="create_group")

groupId = var["Id"]
groupArn = var["Arn"]
groupName = var["Name"]

response = ggClient.create_group_version(
	GroupId=groupId,
	CoreDefinitionVersionArn=coreDefinitionArn,
	FunctionDefinitionVersionArn=functionDefinition,
	SubscriptionDefinitionVersionArn=subscriptionDefinition,
	ResourceDefinitionVersionArn=resourceDefinition
)
var = treatAndLogDict(response, functionName="create_group_version")

groupIdVersion = var["Version"]

#response = client.create_deployment()
