# https://forums.aws.amazon.com/thread.jspa?messageID=831623&tstart=0

import boto3
import json
import ast
import uuid
import logging

# create logger
logger = logging.getLogger("syncCoreShadow.py")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

logger.info("Program started.")

client = boto3.client('greengrass')
group_id = 'b5b18162-50dc-45c9-a8b5-99b651457cad'

###########################################
# Step 1 - Find the latest group version:
###########################################
logger.info("Start Step 1.")

response = client.list_group_versions(GroupId=group_id, MaxResults='1')
var = ast.literal_eval(json.dumps(response))
logger.debug("list_group_version = {}".format(json.dumps(var, indent=2)))

group_version_id = var["Versions"][0]["Version"]
logger.debug(" group_version_id payload: {}".format(group_version_id))

response = client.get_group_version(GroupId=group_id, GroupVersionId=group_version_id)
var = ast.literal_eval(json.dumps(response))
logger.debug("get_group_version payload: {}".format(json.dumps(var, indent=2)))


# Definitions that all hazel cores allways have
coreDefinition = var["Definition"]["CoreDefinitionVersionArn"]
# 		Definitions that will be used on step 4
functionDefinition = var["Definition"]["FunctionDefinitionVersionArn"]
subscriptionDefinition = var["Definition"]["SubscriptionDefinitionVersionArn"]
resourceDefinition = var["Definition"]["ResourceDefinitionVersionArn"]

"""
# Definitions that hazel cores might have > THEY DON'T HAVE YET
	deviceDefinition = var["Definition"]["DeviceDefinitionVersionArn"]
	loggerDefinition = var["Definition"]["LoggerDefinitionVersionArn"]
"""

coreDefinitionList = coreDefinition.split("/")
logger.debug("coreDefinitionList = {}".format(coreDefinitionList))
for i in range(len(coreDefinitionList)):
	if coreDefinitionList[i] == "cores":
		core_id = coreDefinitionList[i + 1]
	if coreDefinitionList[i] == "versions":
		core_version = coreDefinitionList[i + 1]
logger.debug("core_id = {}".format(core_id))
logger.debug("core_version = {}".format(core_version))

##########################################################################
# Step 2 - Get the latest core definition version to chec shadow syncing:
##########################################################################
logger.info("Started Step 2")

response = client.get_core_definition_version(CoreDefinitionId=core_id, CoreDefinitionVersionId=core_version)
logger.debug("get_core_definition_version payload: {}".format(json.dumps(response, indent=2)))

certificate_arn = response["Definition"]["Cores"][0]["CertificateArn"]
thing_arn = response["Definition"]["Cores"][0]["ThingArn"]

###############################################################################
# Step 3 - Create a new core definition version to enable core shadow syncing:
###############################################################################
logger.info("Started Step 3")

random_id = str(uuid.uuid4())
logger.debug("random_id = {}".format(random_id))
newcore = [
	{
		"CertificateArn": certificate_arn,
		"Id": random_id,
		"SyncShadow": True,
		"ThingArn": thing_arn
	}
]

response = client.create_core_definition_version(CoreDefinitionId=core_id, Cores=newcore)
logger.debug("create_core_definition_version payload: {}".format(json.dumps(response, indent=2)))

newCoreDefinition = response["Arn"]


#############################################################################
# Step 4 - Create group definition version with new core definition version:
#############################################################################
logger.info("Started Step 4")

response = client.create_group_version(
	CoreDefinitionVersionArn=newCoreDefinition,
	#DeviceDefinitionVersionArn=deviceDefinition,
	FunctionDefinitionVersionArn=functionDefinition,
	GroupId=group_id,
	#LoggerDefinitionVersionArn=loggerDefinition,
	ResourceDefinitionVersionArn=resourceDefinition,
	SubscriptionDefinitionVersionArn=subscriptionDefinition
)
logger.debug("create_group_version payload: {}".format(json.dumps(response, indent=2)))

new_group_version = response["Version"]

# Deploy new group version
logger.info("Deploying new group version.")

reponse = client.create_deployment(DeploymentType="NewDeployment", GroupId=group_id, GroupVersionId=new_group_version)
logger.debug("create_deployment payload: {}".format(json.dumps(response, indent=2)))

logger.info("Program finished")
